TOP = $(SRCROOT)
DIR = my_rpc

ERL_APP = my_rpc
ERL_DEFINES = MY_RPC_VERSION=\'1.0.0.0\'
ERL_MODULES = \
        my_rpc \
        my_rpc_srv \
        my_rpc_app


include $(TOP)/makeincl/makefile.otp_erlang

