%%%---------------------------------------------------------------------------
%% @doc RPC Server Application (Test Exercise)
%%
%% This module implements interface for RPC Server
%%
%%
%% @copyright 2012 Alert Logic, Inc.
%% @author Alexander Turkin <turkin@reksoft.com>
%%%--------------------------------------------------------------------------

-module(my_rpc).
-author('Alexander Turkin <turkin@reksoft.com>').
-vsn(?MY_RPC_VERSION).


%% API
-export([is_running/0]).
%% supervision startup interface
-export([start_link/0]).
%% Troubleshooting functions
-export([call/4, call/5, block_call/4, block_call/5, async_call/4, yield/1, cast/4]).

%% Include logging and error macro interfaces
%%-include_lib("coresrv/include/logging.hrl").

-define(SERVER, my_rpc_srv).

%%====================================================================
%% API functions
%%====================================================================

%%%--------------------------------------------------------------------------
-spec(is_running/0 :: () -> true | false ).
%%%--------------------------------------------------------------------------
%% @doc Check the process to see if it's running
%% @end
%%%--------------------------------------------------------------------------
is_running() ->
    case erlang:whereis(?SERVER) of
        Pid when is_pid(Pid) ->
            true;
        _ ->
            false
    end.

%%%--------------------------------------------------------------------------
-spec(start_link/0 :: () -> ok ).
%%%--------------------------------------------------------------------------
%% @doc Start the presence gen_server
%% @end
%%%--------------------------------------------------------------------------
start_link() ->
    gen_server:start_link({local, ?SERVER},
                          ?SERVER,
                          [],
                          []).


%% TODO catch exception for calls to not-alive node
call(Node, Module, Function, Args) ->
    call(Node, Module, Function, Args, infinity).
    
call(Node, Module, Function, Args, Timeout) -> 
    %% TODO REIMPLEMENT call
    Pid = gen_server:call({?SERVER, Node}, {call, Module, Function, Args}, Timeout),
    receive %% what if process will exit? maybe we need to flush messagebox?
        {call_func_result, Pid, Result} -> Result
    end.
    %% TODO add timeout support here
    
    
block_call(Node, Module, Function, Args) ->
    block_call(Node, Module, Function, Args, infinity).
    
block_call(Node, Module, Function, Args, Timeout) -> 
    gen_server:call({?SERVER, Node}, {block_call, Module, Function, Args}, Timeout).


async_call(Node, Module, Function, Args) -> %% Key
    gen_server:call(?SERVER, {async_call, Node, Module, Function, Args}).

        
yield(Key) -> % Res | {badrpc, Reason}
    gen_server:call(?SERVER, {yield, Key}).
    
    
cast(Node, Module, Function, Args) ->
    gen_server:cast({?SERVER, Node}, {cast, Module, Function, Args}),
    true.
