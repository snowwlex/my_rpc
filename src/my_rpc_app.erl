%%%---------------------------------------------------------------------------
%% @doc RPC Server Application (Test Exercise)
%%
%% This module implements RPC Server Application/Supervisor
%%
%%
%% @copyright 2012 Alert Logic, Inc.
%% @author Alexander Turkin <turkin@reksoft.com>
%%%--------------------------------------------------------------------------

-module(my_rpc_app).
-author('Alexander Turkin <turkin@reksoft.com>').
-vsn(?MY_RPC_VERSION).

-behaviour(application).
-behaviour(supervisor).

-export([start/0]).
%% Application callbacks
-export([start/2, stop/1]).
%% Supervisor callbacks
-export([init/1]).

%%====================================================================
%% Constants
%%====================================================================
-define(MAX_RESTART, 5).
-define(MAX_RESTART_TIME, 60).


%%====================================================================
%% Public API
%%====================================================================

%%%---------------------------------------------------------------------------
-spec(start/0 :: ( ) -> ok).
%%%---------------------------------------------------------------------------
%% @doc _____________
%%
%% ________________________________________________________
%% ________________________________________________________
%%
start() ->
    coresrv:start(),
    %% then start our app
    coresrv:ensure_app_start(my_rpc).

%%====================================================================
%% Application callbacks
%%====================================================================
%%--------------------------------------------------------------------
%% @hidden
%% Function: start(Type, StartArgs) -> {ok, Pid} |
%%                                     {ok, Pid, State} |
%%                                     {error, Reason}
%% Description: This function is called whenever an application 
%% is started using application:start/1,2, and should start the processes
%% of the application. If the application is structured according to the
%% OTP design principles as a supervision tree, this means starting the
%% top supervisor of the tree.
%%--------------------------------------------------------------------
start(_Type, _StartArgs) ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).

%%--------------------------------------------------------------------
%% @hidden
%% Function: stop(State) -> void()
%% Description: This function is called whenever an application
%% has stopped. It is intended to be the opposite of Module:start/2 and
%% should do any necessary cleaning up. The return value is ignored. 
%%--------------------------------------------------------------------
stop(_State) ->
    ok.

%%====================================================================
%% Supervisor callbacks
%%====================================================================
%%--------------------------------------------------------------------
%% @hidden
%% Func: init(Args) -> {ok,  {SupFlags,  [ChildSpec]}} |
%%                     ignore                          |
%%                     {error, Reason}
%% Description: Whenever a supervisor is started using 
%% supervisor:start_link/[2,3], this function is called by the new process 
%% to find out about restart strategy, maximum restart frequency and child 
%% specifications.
%%--------------------------------------------------------------------
init([]) ->
    %% this specifies the top-level child process for the monitor_extractor_lag
    %% application which manages the ui endpoint services
    %MonitorExtractorLagServer = {
    %    monitor_extractor_lag, {monitor_extractor_lag, start_link, []},
    %    %% Restart strategy = permanent | transient | temporary
    %    permanent,
    %    %% Shutdown strategy = brutal_kill | int() >= 0 | infinity
    %    2000,
    %    %% Child Process Type = worker | supervisor
    %    supervisor,
    %    %% Modules  = [Module] | dynamic
    %    [monitor_extractor_lag, monitor_extractor_lag_srv] 
    %},
    PresenceServer = {
        my_rpc, {my_rpc,
                 start_link,
                 []},
        %% Restart strategy = permanent | transient | temporary
        permanent,
        %% Shutdown strategy = brutal_kill | int() >= 0 | infinity
        2000,
        %% Child Process Type = worker | supervisor
        worker,
        %% Modules  = [Module] | dynamic
        [my_rpc_srv]
    },
    {ok, {{one_for_one, ?MAX_RESTART, ?MAX_RESTART_TIME},
          [PresenceServer]}}.

%%==============================================================================
%%% Internal functions
%%==============================================================================

