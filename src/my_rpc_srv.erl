%%%---------------------------------------------------------------------------
%% @doc RPC Server Application (Test Exercise)
%%
%% This module implements RPC Server functionality
%%
%%
%% @copyright 2012 Alert Logic, Inc.
%% @author Alexander Turkin <turkin@reksoft.com>
%%%--------------------------------------------------------------------------

-module(my_rpc_srv).
-author('Alexander Turkin <turkin@reksoft.com>').
-vsn(?MY_RPC_VERSION).

-behaviour(gen_server).

%% gen_server callbacks
-export([init/1, handle_call/3, handle_cast/2, terminate/2, code_change/3]).

%% internal functions
-export([execute_and_answer/5]).
-export([execute_and_answer2/4]).

-record(state, {keys_list, next_key}).


%%==============================================================================
%% gen_server callbacks
%%==============================================================================

%%------------------------------------------------------------------------------
%% Function: init(Args) -> {ok, State} |
%%                         {ok, State, Timeout} |
%%                         ignore               |
%%                         {stop, Reason}
%% Description: Initiates the server
%%------------------------------------------------------------------------------
init([]) ->
    process_flag(trap_exit, true),
    {ok, #state{keys_list = [], next_key = 0} }.

%%------------------------------------------------------------------------------
%% Function: %% handle_call(Request, From, State) -> {reply, Reply, State} |
%%                                      {reply, Reply, State, Timeout} |
%%                                      {noreply, State} |
%%                                      {noreply, State, Timeout} |
%%                                      {stop, Reason, Reply, State} |
%%                                      {stop, Reason, State}
%% Description: Handling call messages
%%------------------------------------------------------------------------------
   
handle_call({call, Module, Function, Args}, From, State) ->
    %% TODO reimplement, it's acting like block_call now
    %% we should spawn process here and wait for a new message?
    %Pid = spawn(Module, Function, Args),
    Pid = spawn(?MODULE, execute_and_answer2, [From, Module, Function, Args]),
    {reply, Pid, State};
        
handle_call({block_call, Module, Function, Args}, _From, State) ->
    Res = apply(Module, Function, Args),
    {reply, Res, State};
    
handle_call({async_call, Node, Module, Function, Args}, _From, #state{keys_list = KeyAnswers, next_key = NextKey}) ->
    spawn(Node, ?MODULE, execute_and_answer, [node(), NextKey, Module, Function, Args]),
    {reply, NextKey, #state{keys_list = KeyAnswers, next_key = NextKey + 1}};
        
handle_call({yield, Key}, _From, #state{keys_list = KeyAnswers, next_key = NextKey} = State) ->
    io:format("Current state is ~w~n", [KeyAnswers]),
    case lists:keyfind(Key, 1, KeyAnswers) of
        {Key, Answer} ->
            NewKeyAnswers = lists:keydelete(Key, 1, KeyAnswers),
            {reply, Answer, #state{keys_list = NewKeyAnswers, next_key = NextKey}};
        false -> 
            %% TODO make a suspending here, not return immediately
            {reply, {error, no_instance}, State}
    end.
          
%%------------------------------------------------------------------------------
%% Function: handle_cast(Msg, State) -> {noreply, State} |
%%                                      {noreply, State, Timeout} |
%%                                      {stop, Reason, State}
%% Description: Handling cast messages
%%------------------------------------------------------------------------------


handle_cast({cast, Module, Function, Args}, State) ->
    spawn(Module, Function, Args),
    %% TODO MAYBE write something to state here, to know what processes have been 
    %% spawned, for their specific terminating?
    {noreply, State};
        
handle_cast({executed, Key, Res}, #state{keys_list = KeyAnswers, next_key = NextKey}) ->
    NewKeyAnswers = lists:keystore(Key, 1, KeyAnswers, {Key, Res}),
    {noreply, #state{keys_list = NewKeyAnswers, next_key = NextKey}}.
    
%%------------------------------------------------------------------------------
%% Function: handle_info(Info, State) -> {noreply, State} |
%%                                       {noreply, State, Timeout} |
%%                                       {stop, Reason, State}
%% Description: Handling all non call/cast messages
%%------------------------------------------------------------------------------

%%------------------------------------------------------------------------------
%% Function: terminate(Reason, State) -> void()
%% Description: This function is called by a gen_server when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any necessary
%% cleaning up. When it returns, the gen_server terminates with Reason.
%% The return value is ignored.
%%------------------------------------------------------------------------------
terminate(_Reason, _State) ->
    ok.

%%------------------------------------------------------------------------------
%% Func: code_change(OldVsn, State, Extra) -> {ok, NewState}
%% Description: Convert process state when code is changed
%%------------------------------------------------------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%%------------------------------------------------------------------------------
%%% Internal functions
%%------------------------------------------------------------------------------

%% is there a std function for that?
execute_and_answer(Node, Key, Module, Function, Args) ->
    Res = apply(Module, Function, Args),
    gen_server:cast({?MODULE, Node}, {executed, Key, Res}).
    
execute_and_answer2({FromPid, _Tag}, Module, Function, Args) ->
    Res = apply(Module, Function, Args),
    %% is it wrong passing the message there. What about OTP wrappers?
    FromPid ! {call_func_result, self(), Res},
    ok.
